package com.skymap.dwnukowski.skyrise

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


fun ViewGroup.inflate(layoutId: Int, toRoot: Boolean): View = LayoutInflater.from(context).inflate(layoutId, this, toRoot)