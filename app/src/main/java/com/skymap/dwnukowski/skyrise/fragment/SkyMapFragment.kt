package com.skymap.dwnukowski.skyrise.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Looper
import android.support.inject
import android.widget.Toast
import com.github.babedev.dexter.dsl.runtimePermission
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.skymap.dwnukowski.skyrise.client.GooglePlacesApi
import com.skymap.dwnukowski.skyrise.event.OnLocationChanged
import com.skymap.dwnukowski.skyrise.event.OnPlaceRowClick
import com.skymap.dwnukowski.skyrise.event.Rx
import com.skymap.dwnukowski.skyrise.model.Place
import com.skymap.dwnukowski.skyrise.model.PlacesResponse
import org.jetbrains.anko.*


open class SkyMapFragment : SupportMapFragment(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AnkoLogger {

    companion object {
        private val RADIUS = 10000
        private val LIMIT = 3
        private val DEFAULT_ZOOM = 11f
        private val MAX_WAIT_TIME: Long = 12000
        private val INTERVAL_TIME: Long = 7000
        private val USER_TITLE = "ME :)"
    }

    private val googlePlacesApi by inject<GooglePlacesApi>()
    private var lastPlaces = listOf<Place>()
    private var map: GoogleMap? = null
    private val client: FusedLocationProviderClient by lazy { LocationServices.getFusedLocationProviderClient(activity) }
    private val locationCallback: LocationCallback by lazy { buildLocationCallback() }
    private val googleApiClient: GoogleApiClient by lazy { buildGoogleApiClient() }

    override fun onCreate(p0: Bundle?) {
        Rx.register(OnPlaceRowClick::class.java, {
            onPlaceRowClick(it)
        })
        super.onCreate(p0)
    }

    override fun onConnected(p0: Bundle?) {
        info { "google api connected" }
    }

    override fun onResume() {
        requestLocationUpdates()
        super.onResume()
    }

    override fun onPause() {
        client.removeLocationUpdates(locationCallback)
        super.onPause()
    }

    override fun onConnectionSuspended(p0: Int) {
        val msg = "connection suspended"
        warn { msg }
        Toast.makeText(this.context, msg.toUpperCase(), Toast.LENGTH_LONG).show()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        val msg = "connection failed"
        warn { msg }
        Toast.makeText(this.context, msg.toUpperCase(), Toast.LENGTH_LONG).show()
    }

    override fun onMapReady(map: GoogleMap) {
        debug { "on map ready" }
        this.map = map
        map.mapType = GoogleMap.MAP_TYPE_NORMAL

        googleApiClient.connect()
        requestLocationUpdates()
    }

    private fun onPlaceRowClick(it: OnPlaceRowClick) {
        val position = it.result.geometry.location.let { LatLng(it.lat, it.lng) }
        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(position, DEFAULT_ZOOM))
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates() {
        runtimePermission {
            permission(Manifest.permission.ACCESS_FINE_LOCATION) {
                granted {
                    map?.isMyLocationEnabled = true
                    client.requestLocationUpdates(LocationRequest.create()
                            .setMaxWaitTime(MAX_WAIT_TIME)
                            .setInterval(INTERVAL_TIME)
                            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY), locationCallback, Looper.myLooper())
                            .addOnCompleteListener {
                                debug { "location update succeed, on complete listener activated" }
                            }
                            .addOnFailureListener {
                                error("Location update failure", it)
                            }
                }

                denied {
                    warn { "location permission denied" }
                    Toast.makeText(context, "You won;t be able to use this app now", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun buildGoogleApiClient(): GoogleApiClient {
        return GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    private fun buildLocationCallback(): LocationCallback {
        return object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
//                Toast.makeText(activity, "CHANGE", Toast.LENGTH_SHORT).show()
                with(p0.lastLocation) {
                    info { "on location result, lat: $latitude, lng: $longitude" }
                    val position = LatLng(latitude, longitude)
                    googlePlacesApi.askForPlaces(position, RADIUS, { googlePlacesCallback(it, position) })
                }
                super.onLocationResult(p0)
            }

            private fun googlePlacesCallback(it: PlacesResponse, position: LatLng) {
                val places = it.results.drop(1).take(LIMIT) //drop 1 to avoid taking nearest city data
                if (places != lastPlaces) {
                    Rx.publish(OnLocationChanged(places))
                    map?.let {
                        it.clear()
                        drawUser(position)
                        drawPlaces(places)
                        lastPlaces = places
                    }
                }
            }

            private fun drawUser(position: LatLng) {
                map?.addMarker(MarkerOptions()
                        .position(position)
                        .title(USER_TITLE)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(position, DEFAULT_ZOOM))
            }

            private fun drawPlaces(places: List<Place>) {
                places.forEach {
                    val position = it.geometry.location.let { LatLng(it.lat, it.lng) }
                    map?.addMarker(MarkerOptions()
                            .position(position)
                            .title(it.name)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))
                }
            }
        }
    }
}