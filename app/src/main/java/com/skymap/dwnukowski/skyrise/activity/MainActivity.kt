package com.skymap.dwnukowski.skyrise.activity

import android.os.Bundle
import android.support.inject
import android.support.v7.widget.LinearLayoutManager
import com.skymap.dwnukowski.skyrise.R
import com.skymap.dwnukowski.skyrise.adapter.PlacesRecyclerViewAdapter
import com.skymap.dwnukowski.skyrise.client.GooglePlacesApi
import com.skymap.dwnukowski.skyrise.event.OnLocationChanged
import com.skymap.dwnukowski.skyrise.event.Rx
import com.skymap.dwnukowski.skyrise.fragment.SkyMapFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    private val googlePlacesApi by inject<GooglePlacesApi>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initMapFragment()
        initPlacesRecyclerView()
    }

    private fun initPlacesRecyclerView() {
        with(main_placesList) {
            layoutManager = LinearLayoutManager(context)
            adapter = PlacesRecyclerViewAdapter(emptyList(), googlePlacesApi)
            Rx.register(OnLocationChanged::class.java, {
                adapter = PlacesRecyclerViewAdapter(it.entities, googlePlacesApi)
            })
        }
    }

    private fun initMapFragment() {
        val mapFragment = SkyMapFragment()
        instantiateFragment(mapFragment, R.id.main_map)
        mapFragment.getMapAsync(mapFragment)
    }
}