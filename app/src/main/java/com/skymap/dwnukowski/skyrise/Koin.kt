package com.skymap.dwnukowski.skyrise

import com.skymap.dwnukowski.skyrise.client.GooglePlacesApi
import org.koin.android.AndroidModule
import org.koin.dsl.context.Context


class Module : AndroidModule() {
    override fun context(): Context =
            declareContext {
                provide { GooglePlacesApi(applicationContext.getString(R.string.google_maps_key)) }
            }
}