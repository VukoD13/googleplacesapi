package com.skymap.dwnukowski.skyrise

import android.app.Application
import org.koin.Koin
import org.koin.KoinContext
import org.koin.android.KoinContextAware
import org.koin.android.init


class MainApplication : Application(), KoinContextAware {
    private lateinit var context: KoinContext

    override fun getKoin(): KoinContext = context

    override fun onCreate() {
        super.onCreate()
        context = Koin().init(this).build(Module())
    }
}