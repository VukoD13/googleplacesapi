package com.skymap.dwnukowski.skyrise.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.skymap.dwnukowski.skyrise.R
import com.skymap.dwnukowski.skyrise.client.GooglePlacesApi
import com.skymap.dwnukowski.skyrise.event.OnPlaceRowClick
import com.skymap.dwnukowski.skyrise.event.Rx
import com.skymap.dwnukowski.skyrise.inflate
import com.skymap.dwnukowski.skyrise.model.Place
import kotlinx.android.synthetic.main.recycler_placeview.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk25.coroutines.onClick


class PlacesRecyclerViewAdapter(private val places: List<Place>, private val googlePlacesApi: GooglePlacesApi) : RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder>(), AnkoLogger {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.recycler_placeview, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(places[position])
    }

    override fun getItemCount(): Int = places.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Place) {
            with(itemView) {
                item.photos?.run {
                    if (isNotEmpty()) {
                        googlePlacesApi.loadPhoto(this[0].photo_reference, placeview_photo)
                    }
                }

                placeview_nameLabel.text = item.name
                itemView.onClick {
                    Rx.publish(OnPlaceRowClick(item))
                }
            }
        }
    }
}