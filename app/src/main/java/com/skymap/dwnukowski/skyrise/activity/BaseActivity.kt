package com.skymap.dwnukowski.skyrise.activity

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.AnkoLogger


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), AnkoLogger {
    fun instantiateFragment(fragment: Fragment, containerViewId: Int) {
        supportFragmentManager.beginTransaction()
                .add(containerViewId, fragment)
                .commit()
    }
}