package com.skymap.dwnukowski.skyrise.client

import android.widget.ImageView
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.android.gms.maps.model.LatLng
import com.skymap.dwnukowski.skyrise.model.PlacesResponse
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoLogger


class GooglePlacesApi(private val key: String) : AnkoLogger {

    companion object {
        private val GOOGLE_APIS_BASE_URL = "https://maps.googleapis.com/maps/api"
        private val NEARBY_SEARCH_PATH = "/place/nearbysearch"
        private val PLACE_PHOTO_PATH = "/place/photo"
        private val MAX_WIDTH = 300
    }

    fun askForPlaces(position: LatLng, radius: Int, action: (placeResponse: PlacesResponse) -> Unit) {
        "$GOOGLE_APIS_BASE_URL$NEARBY_SEARCH_PATH/json?location=${position.latitude},${position.longitude}&radius=$radius&key=$key"
                .httpGet().responseObject(PlacesResponse.Deserializer()) { request, response, result ->
            when (result) {
                is Result.Failure -> {
                    val error = result.error
                    error { error }
                }
                is Result.Success -> {
                    action(result.value)
                }
            }
        }
    }

    fun loadPhoto(reference: String, imageView: ImageView) {
        Picasso.with(imageView.context)
                .load("$GOOGLE_APIS_BASE_URL$PLACE_PHOTO_PATH?maxwidth=$MAX_WIDTH&photoreference=$reference&key=$key")
                .into(imageView)
    }
}