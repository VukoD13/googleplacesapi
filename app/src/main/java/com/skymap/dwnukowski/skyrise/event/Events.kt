package com.skymap.dwnukowski.skyrise.event

import com.skymap.dwnukowski.skyrise.model.Place


data class OnLocationChanged(val entities: List<Place>)

data class OnPlaceRowClick(val result: Place)