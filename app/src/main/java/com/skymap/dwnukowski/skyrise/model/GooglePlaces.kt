package com.skymap.dwnukowski.skyrise.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson


data class PlacesResponse(val results: List<Place>) {
    class Deserializer : ResponseDeserializable<PlacesResponse> {
        override fun deserialize(content: String): PlacesResponse? = Gson().fromJson(content, PlacesResponse::class.java)
    }
}

data class Place(val geometry: Geometry, val id: String, val name: String, val photos: List<Photo>?)

data class Geometry(val location: Location)

data class Location(val lat: Double, val lng: Double)

data class Photo(val photo_reference: String)