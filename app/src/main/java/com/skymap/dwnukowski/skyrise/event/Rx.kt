package com.skymap.dwnukowski.skyrise.event

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject


object Rx {

    private val publisher = PublishSubject.create<Any>()

    fun publish(event: Any) {
        publisher.onNext(event)
    }

    fun <T> register(eventType: Class<T>, action: (T) -> Unit): Disposable {
        return events(eventType)
                .subscribe(action)
    }

    private fun <T> events(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
}